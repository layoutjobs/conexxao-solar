<!DOCTYPE HTML>
<html lang="pt-br">

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Conexxão Solar é uma empresa que preza pelo crescimento de tecnologia fotovoltaica no Brasil." />

    <!-- OGTitle -->
    <meta property="og:title" content="Conexxão Solar"/>
    <meta property="og:description" content="Conexxão Solar é uma empresa que preza pelo crescimento de tecnologia fotovoltaica no Brasil." />
    <meta property="og:image" content="http://conexxaosolar.com.br/logo.png" />


    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.10/css/uikit.min.css" />

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/img.css">
    <link rel="stylesheet" type="text/css" href="css/media.css">
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:500" rel="stylesheet">

    <!-- FavIcon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <title>Conexxão Solar</title>
</head>

<body>
    <div class="uk-navbar-container">
        <header>
            <nav class="uk-navbar-container uk-margin" uk-sticky="bottom: #offset" uk-navbar>
                <div class="uk-navbar-left">
                    <a class="uk-navbar-item uk-logo" href="">
                        <img width="270" src="img/logo.png" alt="Logo">
                    </a>
                </div>
                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav uk-visible@l">
                        <li><a href="#quemsomos" uk-scroll>Quem Somos</a></li>
                        <li><a href="#vantagens" uk-scroll>Vantagens</a></li>
                        <li><a href="#sustentabilidade" uk-scroll>Sustentabilidade</a></li>
                        <li><a href="#tecnologia" uk-scroll>Tecnologia</a></li>
                        <li><a href="#diferencial" uk-scroll>Diferencial</a></li>
                        <li><a href="#contato" uk-scroll>Fale Conosco</a></li>
                    </ul>
                    <a class="uk-navbar-toggle uk-hidden@l" uk-navbar-toggle-icon uk-toggle="target: #offcanvas-nav-primary"></a>
                </div>
            </nav>
        </header>
    </div>

    <main>
        <!-- Banner Principal -->
        <section id="banner">
            <div class="uk-container-expand">
                <div>
                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(img/banner_home.png); height: 700px;">
                        <div class="quadrado">
                            <p class="title">ENERGIA SOLAR</p>
                            <p class="sub-title">MAIS ECONOMIA. MAIOR SUSTENTABILIDADE.</p>
                            <hr style="width: 20%; background-color: #ff961b;height: 5px;margin: auto;">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Fotos-->
        <section id="fotos">
            <div class="uk-container">
                <img data-src="img/banner_quem_somos.png" width="" height="" alt="" uk-img>
            </div>
        </section>

        <!--Quem Somos-->
        <section id="quemsomos">
            <div class="uk-container">
                <h1>SOLUÇÕES EM ENERGIA SOLAR</h1>
                <p class="quem-somos">QUEM SOMOS</p>
                <hr style="width: 10%; background-color: #ff961b;height: 10px;margin: auto; margin-top: -10px!important;">
                <br>
                <p class="conexxao">A Conexxão Solar é uma empresa que preza pelo crescimento da tecnologia fotovoltaica no Brasil. Localizados
                    em Salto, no interior de São Paulo, nossa empresa atende uma demanda tanto comercial, quanto residencial,
                    levando aos nossos clientes, uma energia de forma limpa e econômica, através da instalação de painéis
                    de energia solar fotovoltaica.</p>
                    <br>
                    <br>
                    <br>
                    <br>
            </div>
        </section>

        <!-- Comentário -->
        <section id="comentario">
            <div class="uk-container-left">
                <img id="banner1" src="img/banner_laranja.png" alt="Banner-Comentarios">
            </div>
            <div class="uk-container">
                <p class="energia">A geração de energia limpa e renovável gera economia, que será sentida logo no primeiro mês de instalação dos painéis fotovoltaicos.</p>
                <p class="sub-energia"><i>O investidor produz e se torna o dono da própria energia que consome.</i></p>
            </div>
        </section>

        <!-- Vantagens -->
        <section id="vantagens">
            <div class="uk-container">
                <h1>VANTAGENS DO SISTEMA</h1>
                <hr style="width: 15%; background-color: #ff961b;height: 5px;margin: auto;">
                <p class="vntg">A Conexxão Solar oferece uma grande alternativa para o setor da construção civil. Com uma fonte de energia limpa e infinita, o sol, reduzindo significativamente os custos com energia elétrica e alcançando grande eficiência no uso da energia solar fotovoltaica.</p>
            </div>
            <div id="icons" class="uk-child-width-expand@s uk-text-center" uk-grid>
                <div>
                    <div id="icone" class="uk-card">
                        <img src="img/icon_01.png" alt="icon1">
                        <p>Redução na Conta de Energia</p>
                    </div>
                </div>
                <div>
                    <div id="icone" class="uk-card">
                        <img src="img/icon_02.png" alt="icon2">
                        <p>Redução no Risco de Sobrecarga</p>
                    </div>
                </div>
                <div>
                    <div id="icone" class="uk-card">
                        <img src="img/icon_03.png" alt="icon3">
                        <p>Retorno do Investimento</p>
                    </div>
                </div>
                <div>
                    <div id="icone" class="uk-card">
                        <img src="img/icon_04.png" alt="icon4">
                        <p>Redução da Emissão de Co2</p>
                    </div>
                </div>
                <div>
                    <div id="icone" class="uk-card">
                        <img src="img/icon_05.png" alt="icon5">
                        <p>Valorização do Imóvel</p>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
        </section>

        <!-- Sustentabilidade -->
        <section id="sustentabilidade">
            <div class="uk-container-expand">
                <div id="banner-sustentavel" class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle">
                    <div class="uk-container">
                        <h2 class="stnb">SUSTENTABILIDADE</h2>
                        <img id="sustentavel" data-src="" width="img/sustentabilidade.png" height="" alt="" uk-img>
                        <hr id="line-sust">
                        <p class="text">A Conexxão Solar é uma empresa brasileira, em um país tropical, onde o sol é abundante durante todo o ano. Os painéis fotovoltaicos representam uma fonte de energia limpa, renovável e infinita, quanto à sua fonte, pois são alimentados pela luz solar.</p>
                        <p class="sub-text">Além da economia na conta de energia, a Energia Solar Fotovoltaica representa a sustentabilidade e o futuro, onde preservar a vida ambiental é fator decisivo para as futuras gerações. </p>
                        <br>
                        <br>
                        <br>
                        <br>
                        <img id="sustentabilidade-mobile" src="img/sustentabilidade2.png" alt="">
                    </div>
                </div>
            </div>
        </section>

        <!-- Tecnologia -->
        <section id="tecnologia">
            <div class="uk-container">
                <div class="uk-child-width-expand@s uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card">
                            <h3>TECNOLOGIA</h3>
                            <hr style="width: 20%; background-color: #ff961b;height: 5px;margin: auto;">
                            <p>A Conexxão Solar apresenta projetos de energia solar fotovoltaica para residências e comércios. A classificação chamada de microgeração e minigeração é a responsável pela geração de energia elétrica que vai atender os pequenos e médios consumidores, como residências e comércios, diminuindo assim o consumo junto à rede elétrica de energia. </p>
                            <p>Confira como funciona a microgeração: <br>
                                1.) O painel fotovoltaico instalado pela Conexão Solar recebe a luz do sol e transforma-a em energia elétrica (cc);</p>
                            <p>2.) Um inversor solar converte a energia elétrica (cc) em energia elétrica (ca), que pode ser utilizada em qualquer equipamento de empresa ou residência;</p>
                            <p>3.) A energia do inversor é distribuída para os equipamentos elétricos, diminuindo assim, a energia adquirida através das distribuidoras; </p>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <img id="painel" data-src="img/painel.png" width="" height="" alt="" uk-img>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Diferencial -->
        <section id="diferencial">
            <div class="uk-container-expand">
                <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(img/diferencial.png); height: 720px; ">
                    <div class="quadrado-laranja">
                        <p class="title">DIFERENCIAL</p>
                        <hr style="width: 20%; background-color: #ff961b;height: 5px;margin: auto;">
                        <p class="dif">Por sermos uma empresa de pequeno porte localizada no interior de São Paulo, a Conexxão Solar se diferencia por ter uma alta tecnologia e apresentar a melhor relação custo-benefício para nossos clientes.</p>
                        <p class="dif">Atendendo a projetos residenciais e comerciais, a Conexxão Solar se destaca ainda por apresentar a melhor solução em energia solar ao cliente, podendo apresentar o orçamento adequado para qualquer projeto. </p>
                    </div>
                </div>
            </div>
        </section>

        <!-- Beneficios -->
        <section id="beneficios">
            <div class="uk-container">
                <h1>BENEFÍCIOS DO SISTEMA DE ENERGIA SOLAR</h1>
                <hr style="width: 10%; background-color: #ff961b;height: 5px;margin: auto;">
                <br>
                <br>
                <div id="locais" class="uk-child-width-expand@s uk-text-center" uk-grid>
                    <div id="margin">
                        <div id="segmentos" class="uk-card">
                            <img src="img/residencial.png" alt="Residencia">
                            <p style="font-size: 20px;">RESIDENCIAL</p>
                            <p>Após a instalação das placas solares a conta de energia elétrica residencial já começa a reduzir. O retorno total do investimento pode se dar a partir de 36 meses, dependendo da tarifa cobrada pela concessionária.</p>
                        </div>
                    </div>
                    <div>
                        <div id="segmentos" class="uk-card">
                            <img src="img/industrial.png" alt="Industrial">
                            <p style="font-size: 20px;">INDUSTRIAL</p>
                            <p>No comércio e na indústria o sistema auxilia muito na redução dos custos com energia, além de prevenir sobrecargas. A energia solar não sofre aumento no custo ou taxas como a energia elétrica convencional.</p>
                        </div>
                    </div>
                    <div>
                        <div id="segmentos" class="uk-card">
                            <img src="img/fazenda.png" alt="Fazenda">
                            <p style="font-size: 20px;">FAZENDA/SÍTIO</p>
                            <p>Na área rural o investimento além de gerar economia também está em conformidade com as normas de sustentabilidade. Devido a abundância de “sol” no campo em muitos casos o investidor passa a armazenar energia. </p>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="btn">
                    <a class="uk-button uk-button-default" href="#contato" uk-scroll>FALE CONOSCO</a>
                </div>
            </div>
            <br>
            <br>
            <br>
        </section>
    </main>

    <footer>
        <!-- Fale Conosco -->
        <section id="contato">
            <div class="uk-container">
                <br>
                <br>
                <h4>CONTATO</h4>
                <br>
                <div class="uk-child-width-expand@s uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card contato">
                            <p>Depto. Comercial <a style="color:#ff961b" href="tel:11945739065">11 9 4573 9065</a></p>
                            <p>Depto. Técnico <a style="color:#ff961b" href="tel:11945683252">11 9 4568 3252</a></p>
                            <p>Escritório <a style="color:#ff961b" href="tel:1144564591">11 4456 4591</a></p>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            JOSÉ ALMEIDA TEIXEIRA, 648 <br> JARDIM MARIA JOSÉ <br> CEP 13.321-020 / SALTO / SP
                        </div>
                    </div>
                 </div>
                <br>
                <br>
                <br>
            </div>
        </section>

        <section id="mapa">
            <div class="uk-container-expand">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3667.298704503768!2d-47.30193908448359!3d-23.195782384865623!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf52037f81ffbd%3A0xa334b1d78ef4ffff!2sR.+Jos%C3%A9+de+Almeida+Teixeira+Filho%2C+648+-+Vila+Teixeira%2C+Salto+-+SP%2C+13321-040!5e0!3m2!1spt-BR!2sbr!4v1534791324605" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </section>

        <section id="form">
            <div class="uk-container">
                <br>
                <br>
                <p style="text-align:center; font-size: 25px;">Tire suas dúvidas, escreva abaixo e breve retornaremos.</p>
                <p style="text-align:center; font-size: 15px;">Todas as mensagens serão lidas e o retorno será realizado o mais breve possível.</p>
                
                <form class="formphp" name="form" action="api/send-contact.php" method="post">
                    <div class="uk-margin">
                        <input class="uk-input uk-width-1-1" type="text" name="nome" placeholder="Seu Nome" required>
                    </div>
                    <div class="uk-margin">
                        <input class="uk-input uk-width-1-1" type="text" name="email" placeholder="Seu E-mail" required>
                    </div>
                    <div class="uk-margin">
                        <input class="uk-input uk-width-1-1" type="text" name="assunto" placeholder="Assunto" required>
                    </div>
                    <div class="btn-enviar">
                        <button id="btn-envio" type="submit" class="uk-button">enviar</button>
                    </div>
                </form>
            </div>
        </section>
        <div class="uk-container">
            <p class="copy"><i>Conexxão Solar - Soluções em energia solar. Todos os direitos reservados.</i></p>
            <br>
            <br>
        </div>
    </footer>





    <!-- Menu Responsivo -->
    <div class="uk-offcanvas-content">
        <div id="offcanvas-nav-primary" uk-offcanvas="overlay: true">
            <div class="uk-offcanvas-bar uk-flex uk-flex-column uk-offcanvas-close" style="top: 0px; width: 78%;">
                <a style="margin-top: 17px;" class="uk-offcanvas-brand" href="">
                    <img src="img/logo.png" alt="Logo">
                </a>
                <ul class="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical">
                    <li><a href="#quemsomos" uk-scroll>Quem Somos</a></li>
                    <li><a href="#vantagens" uk-scroll>Vantagens</a></li>
                    <li><a href="#sustentabilidade" uk-scroll>Sustentabilidade</a></li>
                    <li><a href="#tecnologia" uk-scroll>Tecnologia</a></li>
                    <li><a href="#diferencial" uk-scroll>Diferencial</a></li>
                    <li><a href="#contato" uk-scroll>Fale Conosco</a></li>
                </ul>
            </div>
        </div>
    </div>

    

    <!-- JS UIkit -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.10/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.10/js/uikit-icons.min.js"></script>
</body>

</html>