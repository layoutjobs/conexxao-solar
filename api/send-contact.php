<?php

$name = !empty($_POST['nome']) ? filter_var($_POST['nome'], FILTER_SANITIZE_STRING) : null;
$email = !empty($_POST['email']) ? filter_var($_POST['email'], FILTER_SANITIZE_STRING) : null;
$assunto = !empty($_POST['assunto']) ? filter_var($_POST['assunto'], FILTER_SANITIZE_STRING) : null;

if (!$name || !$email) {
    echo '<script>alert("Preencha todos os campos e tente novamente"); history.go(-1);</script>';
} else {
    date_default_timezone_set('Brazil/East');
    
    require 'smtp.php';

    $smtp->host = 'smtp.layoutnet.com.br';
    $smtp->user = 'igor@layoutnet.com.br';
    $smtp->pass = 'Igor75995715';

    $date = date('d/m/Y');
    $hour = date('H:i');

    $msg  = "Olá.<br /><br />";
    $msg .= "\"{$name}\" enviou uma mensagem pelo site.<br /><br />";
    $msg .= "E-mail ou telefone: {$email}.<br /><br />";
    $msg .= "Assunto: {$assunto}. <br /><br />";
    $msg .= "Enviado em {$date} às {$hour}.";
    

    $success = $smtp->send('igor@layoutnet.com.br', "Layout - Mensagem enviada por \"{$name}\"", $msg);

    if (!$success) {
        echo '<script>alert("Sua mensagem foi enviada com sucesso. Obrigado!"); history.go(-1);</script>';
    } else {
        echo '<script>alert("Erro ao enviar a mensagem. Por favor, tente novamente mais tarde."); history.go(-1);</script>';
    }
}